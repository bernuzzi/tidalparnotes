# BibTeX References 

- a bib files is available from (read-only) 

  $ git clone https://bernuzzi@bitbucket.org/bernuzzi/bibtexrefs.git
  
- you can check out the above and set a sym link where necessary

  $ ln -s PATH/TO/bibtexrefs/refs.bib PATH/TO/TEX/references.bib

  alternatively, just add the INSPIRE key in the tex: I will fix the
  refs afterwards.

- use INSPIRE keys everytime is possible for the citations in the LaTeX document, I will update the above bib file accordingly



