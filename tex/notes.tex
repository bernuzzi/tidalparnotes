\documentclass[prd,aps,a4paper,floatfix,notitlepage]{revtex4-1}

%% SB: Note for comments
%% I have a script to automatically remove the following type of
%% comments from the tex at submission time:
%% use two %
%% use a space after %% write here

%% packages
\usepackage{graphicx,psfrag}
\usepackage{mathrsfs,amsmath,amsfonts,amssymb}
\usepackage{multirow}
\usepackage{comment,hyperref}
\usepackage{float}

%% macros
\newcommand{\be}{\begin{equation}}
\newcommand{\ee}{\end{equation}}
\newcommand{\bsube}{\begin{subequations}}
\newcommand{\esube}{\end{subequations}}

\def\l{\ell}
\def\blam{\bar{\lambda}}
\def\Lam{\Lambda}
\def\Lamt{\tilde{\Lambda}}
\def\dLamt{\tilde{\delta{\Lambda}}}
\def\ct{\tilde{c}}

%% macros for comments
\usepackage{color}
%% colors
\definecolor{cyan}{rgb}{0,0.9,0.9}
\definecolor{orange}{rgb}{0.9,0.5,0}
\definecolor{magenta}{rgb}{1,0,1}
\definecolor{purple}{rgb}{0.8,0.4,0.8}
\definecolor{gray}{rgb}{0.8242,0.8242,0.8242}
%%
\newcommand{\bs}[1]{{\textcolor{green}{\texttt{SB: #1}} }}

%% +++++++++++++++++++++++++++++++++++++++++++++++++++++++
\begin{document}

\title{Notes on tidal parameters} 

\author{Sebastiano \surname{Bernuzzi}$^{1,2}$, ...}
\affiliation{${}^1$Department of Mathematical, Physical and Computer Sciences, University of Parma, I-43124 Parma, Italy}
\affiliation{${}^2$Istituto Nazionale di Fisica Nucleare, Sezione Milano Bicocca, gruppo collegato di Parma, I-43124 Parma, Italy}

\date{\today}

\begin{abstract}
A quick manual about tidal parameters in GR. 
\end{abstract}

%% \pacs{
%%   % 04.25.D-,   % numerical relativity
%%   % 
%%   04.30.Db,   % gravitational wave generation and sources
%%   % 04.40.Dg,   % Relativistic stars: structure, stability, and oscillations
%%   % 04.70.Bw,   % classical black holes
%%   95.30.Sf,     % relativity and gravitation
%%   % 
%%   % 95.30.Lz,   % Hydrodynamics
%%   %
%%   97.60.Jd      % Neutron stars
%%   % 97.60.Lf    % black holes (astrophysics)
%%   % 98.62.Mw    % Infall, accretion, and accretion disks
%% }

%%
\maketitle

%% \tableofcontents

%% Template for figures
%% \begin{figure}[t]
%%   \centering 
%%     \includegraphics[width=0.49\textwidth]{../fig/ }
%%     \caption{ }
%%  \label{fig: }
%% \end{figure}

%% Template for tables
%% \begin{table}[t]
%%   \centering    
%%   \caption{ }
%%   \begin{tabular}{ccc}        
%%     \hline
%%     col$1$ & col$2$ & col$3$ \\
%%     \hline
%%     &  &  \\
%%     \hline
%%   \end{tabular}
%%  \label{tab: }
%% \end{table}

%% ________________________________________________________
%% \section{Introduction}

%% ________________________________________________________
\section{Definitions}

Label $A,B$ the two isolated stars. Star $A$ has
gravitational mass $M_{A}$, areal radius $R_A$, compactness $C_A$.
The tidal interactions of a star (e.g.~star A) in an external tidal field are
in general parametrized by (See \cite{Damour:2009wj} and refs therein)
(i) gravito-electric coefficients
$G\mu_\l$ $[length]^{2\l+1}$ measuring the $\l$th-order mass
multipolar moment induced in the star by the external $\l$th-order
gravito-electric field; 
(ii) gravito-magnetic coefficients $G\sigma_\l$ $[length]^{2\l+1}$
measuring the $\l$th-order spin multipolar moment induced in the
star by the external $\l$th-order gravito-magnetic field; 
(iii) shape coefficients $h_\l$ measuring the distorsion of the
surface of the star by an external $\l$th-order gravito-electric 
field. 
%
In the literature, it is customary to consider only the
dominant gravito-electric interactions (i) and, in most of the cases,
only the leading order term $\l=2$. 

The dimensionless relativistic Love numbers are then defined as
\be \label{eq:Lovenum}
k_\l^A = \frac{(2\l-1)!!}{2}\frac{G\mu^A_\l}{R^{2\l+1}_A} \ \ , \ 
j_\l^A = \frac{(2\l-1)!!}{2}\frac{G\sigma^A_\l}{R^{2\l+1}_A} \ .
\ee
For a black hole one sets $\mu_\l^{\rm BH}=\sigma_\l^{\rm BH}=0$
\cite{Damour:2009wj,Binnington:2009bb,Damour:2009va}. 
The notation above follows from \cite{Damour:2009vw}.
$\mu_2$ is simply called $\lambda$ in
\cite{Flanagan:2007ix,Hinderer:2009ca,Vines:2010ca}. Ref.~\cite{Vines:2010ca}
uses $\Lambda$ for $\lambda$. Ref.~\cite{Hinderer:2007mb}
indicates $k_2$ as $\lambda$. 
\\

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

For a binary system assume $M_A\geq M_B$ and define the mass ratio as
\be
q=M_A/M_B\geq1 \ .
\ee 
The mass of the binary is $M=M_A+M_B$, the symmetric
mass-ratio 
\be
\nu=M_AM_B/M=q/(1+q)^2\in[0,1/4] \ .
\ee
Tidal interactions in the post-Newtonian and EOB framework are
parametrized by the {\it dimensionless tidal coupling constants}, 
\bsube
\label{kappal}
\begin{align}
\kappa^A_\ell &= 2 \frac{M_BM_A^{2\l}}{M^{2\l+1}}\frac{k^A_\l}{C_A^{2\l+1}}
= 2 q^{-1} \left(\frac{X_A}{C_A}\right)^{2\l+1} k^A_\l \ , \\
\kappa^B_\ell &= 2 \frac{M_AM_B^{2\l}}{M^{2\l+1}}\frac{k^B_\l}{C_B^{2\l+1}}
= 2 q \left(\frac{X_B}{C_B}\right)^{2\l+1} k^B_\l  \ ,
\end{align}
\esube
where $X_{A,B}=M_{A,B}/M$. Specializing to the quadrupolar case 
$\l=2$, and using $X_A=q/(1+q)$ and $X_B=1-X_A=1/(1+q)$ one has
\bsube
\label{kappa2}
\begin{align}
\kappa^A_2 &= 2 \frac{q^4}{(1+q)^5} \frac{k^A_2}{C_A^5} \ , \\
\kappa^B_2 &= 2 \frac{q}{(1+q)^5} \frac{k^B_2}{C_B^5} \ . 
\end{align}
\esube
The sum of the above two numbers
\be
\kappa_2^T = \kappa^A_2 + \kappa^B_2 \ ,
\ee
parametrize at leading order the tidal GW as well as the dynamics
(cf. EOB Hamiltonian \cite{Damour:2009wj}).
For example, the 2.5PN SPA tidal phase (Appendix B
of~\cite{Damour:2012yf}) can be written as 
\be\label{eq:PsiT} 
\Psi_T^{\rm SPA} = -\kappa^T_2\frac{\ct_{\rm Newt}}{X_AX_B}
x^{5/2}\ \times ( {\rm PN\ corrections})
\ee 
with $\ct_{\rm Newt} = 39/16$.
\\

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

A slightly different parametrization is typically used in GW data
analysis~\cite{Favata:2013rwa,Wade:2014vqa,Lackey:2014fwa}. 
First, one introduces 
\bsube
\label{Lambda}
\be
\Lam_A = \frac{2}{3} \frac{k^A_2}{C_A^5} \ \ {\rm (\l=2)} \ , 
%\Lam_B = \frac{2}{3} \frac{k^A_2}{C_A^5} \ , 
\ee
\esube
where for the star-labels of the $\Lambda$'s it is often usesd $(1,2)$
instead of $(A,B)$.
Note $\Lambda\propto k_2 C^{-5}$, are essentially the $q$-independent part
of~\eqref{kappa2}; hence the Eq.~\eqref{kappa2} can be expressed as
\be
\kappa^A_2 = 3 \frac{X_A}{X_B} \, X_A^5 \, \Lambda_A \ .
\ee
Ref.~\cite{Yagi:2013sva} additionally defines, for each stars,
the multipolar parameters
\be
\label{barlam}
\blam^A_\l= \frac{2 k^A_\l }{(C^{2\l+1}_A (2\ell-1)!!))} \ ,
\ee
that reduce to $\blam^A_2=\Lambda_A$ for $\l=2$. The multipolar
$\kappa^{A,B}_\l$ can be expressed as
\bsube
\begin{align}\label{kappa_barlam}
\kappa^A_\l &= q^{-1}\ X_A^{2\l+1}\ (2\l-1)!! \ \blam^A_\l \\ 
\kappa^B_\l &= q    \ X_B^{2\l+1}\ (2\l-1)!! \ \blam^A_\l \ .
\end{align}
\esube
Note that $(2\l-1)!!$ for $l=2,3,4$ gives $3, 15, 105$.

Then, one consider the rotation $(\Lam_A,\Lam_B)\to(\Lam_A\pm\Lam_B)$ and
re-express the (2PN, F2) waveforms in terms of
\begin{widetext}
\bsube
\label{Lambdat}
\begin{align}
\Lamt &= \frac{8}{13}\left[ 
  \left(1+7\nu-31\nu^2\right) \left(\Lam_A+\Lam_B\right)
  + \sqrt{1-4\nu} \left(1+9\nu-11\nu^2\right) \left(\Lam_A-\Lam_B\right) 
  \right] \ ,
\\
\dLamt &= \frac{1}{2}\left[
  \sqrt{1-4\nu}
  \left(1-\frac{13272}{1319}\nu+\frac{8944}{1319}\nu^2\right)
  \left(\Lam_A+\Lam_B\right) 
  + \sqrt{1-4\nu}
  \left(1-\frac{15910}{1319}\nu+\frac{32850}{1319}\nu^2+\frac{3380}{1319}\nu^3\right)
  \left(\Lam_A-\Lam_B\right)  
\right] \ .
\end{align}
\esube
\end{widetext}
The particular $\dLamt$ definition follows
from~\cite{Wade:2014vqa,Lackey:2014fwa}. Parameters like
$\kappa^{A,B}_2$ or $\Lam_{A,B}$ are ``highly correlated'' in the sense
their values are of the same order of magnitude, e.g.~$\Lam_A=\Lam_B$
for $q=1$. On the contrary, using $(\Lamt,\dLamt)$ the tidal F2
waveform depends at leading order only on $\Lamt$, and the other
parameter is smaller and might be even ignored, $\dLamt/\Lamt\sim0.01$
($\dLamt=0$ for $q=1$)~\cite{Favata:2013rwa}. 
\\

%% The correlation between compactness and $\Lambda_{A}$ is shown in
%% Fig.~\ref{fig:CLambdaCorrelation}. A fitting model 
%% \be
%% \label{c_fit}
%% C^\text{fit}(\Lam) = \frac{1}{2}
%% \frac{( 1 + p_1 \Lam + p_2 \Lam^2 + p_3 \Lam^3 )}
%%      {( 1 + p_4 \Lam + p_6 \Lam^2 + p_6 \Lam^3)} \ ,  
%%      \ee
%%      with
%%   $\mathbf{p}=(p_1,p_2,...,p_6)=(5.7406e-01,5.4832e-03,1.3196e-06,1.0234e+00,1.5832e-02,7.5585e-06)$
%%      reproduces all the $C_A$ of the sample within $5-8\%$. 
 
%% The correlation between Love
%% numbers (for both stars) is shown in
%% Fig.~\ref{fig:LoveNumberCorrelation}. The fitting model for $\l=3,4$
%% is 
%% \be
%% \label{kell_fit}
%% k_\l^\text{fit}(k_2) = c^{(1)}_\l k_2 + c^{(2)}_\l k_2^2 \ , 
%% \ee
%% with $\mathbf{c}_3=(c^{(1)}_3,c^{(2)}_3)=(0.21074,0.66252)$ 
%% and $\mathbf{c}_4=(0.053493,0.51241)$. All the
%% sample can be reproduced within $15-30\%$.

%% \begin{minipage}{\linewidth}
%%   \centering
%%   \begin{minipage}{0.45\linewidth}
%% %
%% \begin{figure}[H]
%%   \centering 
%%   \includegraphics[width=\linewidth]{../fig/CompactnessLambdaCorrelation.png}
%%   \caption{Correlation between compactness and quadrupolar tidal
%%     parameter $\Lambda_A$ and for a
%%     sample of 14 EOSs and (for each EOS) star masses $M_\odot<M_A<M_{max}$. 
%%     EOS sample: MPA1, 2B, 2H, HB, SLY, APR4, FPS, ENG, ALF2, BGN1H1,
%%     H3, MS1, MS1B, H4. Each EOS is represented by a piecewise
%%     polytropic fit with 4 pieces. In the 
%%     plot, each EOS is shown with a different color code. Solid lines
%%     shows the best fits to \eqref{c_fit}; the fit parameters are 
%%     $\mathbf{p}=(5.7406e-01,5.4832e-03,1.3196e-06,1.0234e+00,1.5832e-02,7.5585e-06)$ .}
%%   \label{fig:CLambdaCorrelation}
%% \end{figure}
%% %
%%   \end{minipage}
%%   \hspace{0.05\linewidth}
%%       \begin{minipage}{0.45\linewidth}
%% % 
%% \begin{figure}[H]
%%   \centering 
%%   \includegraphics[width=\linewidth]{../fig/LoveNumbersCorrelation_k2_vs_k34.png}
%%   \caption{Correlation between Love numbers $k_{3,4}$ and $k_2$ for a
%%     sample of 14 EOSs and (for each EOS) star masses $M_\odot<M_A<M_{max}$. 
%%     EOS sample: MPA1, 2B, 2H, HB, SLY, APR4, FPS, ENG, ALF2, BGN1H1,
%%     H3, MS1, MS1B, H4. Each EOS is represented by a piecewise
%%     polytropic fit with 4 pieces. In the 
%%     plot, each EOS is shown with a different color code. Solid lines
%%     shows the best fits to \eqref{kell_fit}; the fit parameters are 
%%     $\mathbf{c}_3=(2.1961e-01, 5.9921e-01)$ for $\l=3$, and
%%     $\mathbf{c}_4=(5.7023e-02,4.8950e-01)$ for $\l=4$.}
%%   \label{fig:LoveNumberCorrelation}
%% \end{figure}
%% %
%%       \end{minipage}
%% \end{minipage}




%% ________________________________________________________
%\section{Tides or not tides}


%% ________________________________________________________
%\section{ }


%% ________________________________________________________
%\begin{Acknowledgments}
%\end{acknowledgments}

%% ________________________________________________________
%\bibliographystyle{revtex}     
\bibliography{references}

%% +++++++++++++++++++++++++++++++++++++++++++++++++++++++
\end{document}
